#include "matrix.h"

void cleanUp(){
	scanf_s("%*[^\n]");
}

struct VectorN addVectors(struct VectorN* vector1, struct VectorN* vector2)
{
	struct VectorN resVector;
	for (int i = 0; i < N; i++) {
		resVector.arrayV[i] = vector1->arrayV[i] + vector2->arrayV[i];
	}
	
	return resVector;
}

struct MatrixNN addMatrices(struct MatrixNN* matrix1,struct  MatrixNN* matrix2)
{
	struct MatrixNN resMatrix;
	for(int i=0; i<N; i++){
		for (int k=0; k<N; k++)
		{
			resMatrix.arrayM[i][k] = matrix1->arrayM[i][k] + matrix2->arrayM[i][k];
		}
	}
	
	return resMatrix;
}

struct VectorN scaleVector( struct VectorN* vector, float scalar)
{
	struct VectorN resVector;
	for (int i=0; i<N; i++)
	{
		resVector.arrayV[i] = vector->arrayV[i] * scalar;
	}
	
	return resVector;
}

struct MatrixNN scaleMatrix(struct MatrixNN* matrix, float scalar)
{
	struct MatrixNN resMatrix;
	for(int i=0; i<N; i++){
		for (int k=0; k<N; k++)
		{
			resMatrix.arrayM[i][k] = matrix->arrayM[i][k] * scalar;
		}
	}
	
	return resMatrix;
}

struct VectorN multiplyMatrixVector(struct MatrixNN* matrix, struct  VectorN* vector)
{
	struct VectorN resVector;
	resVector.arrayV[0] = 0.0;
	for(int i=0; i<N; i++){
		for (int k=0; k<N; k++)
		{
			resVector.arrayV[i] = resVector.arrayV[i]+ matrix->arrayM[i][k] * vector->arrayV[k];
		}
	}
	
	return resVector;
}

struct MatrixNN multiplyMatrices( struct MatrixNN* matrix1, struct MatrixNN* matrix2)
{
	struct MatrixNN resMatrix;
	resMatrix.arrayM[0][0] = 0.0;
	for (int i=0; i<N;i++)
	{
		for (int k = 0; k < N; k++) {
			resMatrix.arrayM[i][k] = resMatrix.arrayM[i][k] + matrix1->arrayM[i][k] * matrix2->arrayM[k][i];
		}
	}

	return resMatrix;
}

struct VectorN inputVector()
{
	struct VectorN resVector;
	printf_s("Enter your vector: \n");
	for (int i=0; i<N; i++)
	{
		scanf_s("%f", &resVector.arrayV[i]);
	}
	cleanUp();
	return resVector;
}

struct MatrixNN inputMatrix()
{
	struct MatrixNN resMatrix;
	
	printf_s("Enter your matrix: \n");
	for (int i=0; i<N; i++)
	{
		for (int k=0; k<N; k++)
		{
			scanf_s("%f", &resMatrix.arrayM[i][k]);
		}

	}
	cleanUp();
	return resMatrix;
}

void outputVector(struct VectorN* vector)
{
	
	for (int i=0; i<N; i++)
	{
		printf_s("%f \n", vector->arrayV[i]);
	}
	
}

void outputMatrix(struct MatrixNN* matrix)
{
	
	for (int i=0; i<N; i++)
	{
		for (int k=0; k<N; k++)
		{
			printf_s("%f  ", matrix->arrayM[i][k]);
		}
		printf_s("\n");
	}

	
}

void isDiagonal(struct MatrixNN* matrix)
{
	bool kontrolle=true;
	int i, k;

	for (i=0; i<N; i++){
		for (k = 0; k < N; k++){
			if (i == k && matrix->arrayM[i][k] != 0 ){
				continue;
			}
			else if (i != k && matrix->arrayM[i][k] == 0){
				continue;
			}
			
			else {
				kontrolle=false;
				break;
			}	
		}
		if (!kontrolle){
		printf_s("The Matrix is not diagonal");
			break;
		}
		
	}
	if (kontrolle) {
	printf_s("The Matrix is diagonal");
	}
	
}

void isIdentity(struct MatrixNN* matrix)
{
	int i, k;
	bool kontrolle = true;
	for (i=0; i<N; i++)
	{
		for (k=0; k<N; k++)
		{
			if (i==k && matrix->arrayM[i][k]==1)
			{
				continue;
			}
			else if(i!=k && matrix->arrayM[i][k]==0){
				continue;
			}
			else{
				kontrolle = false;
				break;
			}
			
		}
		if (!kontrolle)
			break;
	}
	if (!kontrolle) {
		printf_s("The matrix isn't identity");
	}
	else{
		printf_s("The matrix is identity");
	}
}

void isUpperTriangular(struct MatrixNN * matrix)
{
	int i, k;
	bool kontrolle = true;
	for(i=0;i<N; i++){
		for (k=0;k<N; k++)
		{
			if (k > i && matrix->arrayM[i][k] != 0) {
				kontrolle = false;
				break;
			}
			else {
				continue;
			}
		}
		if (!kontrolle)
			break;
	}
	if (!kontrolle) {
		printf_s("The matrix isn't upper triangular");
	}
	else {
		printf_s("The matrix is upper triangular");
	}
}

void isLowerTriangular(struct MatrixNN* matrix)
{
	int i, k;
	bool kontrolle = true;
	for (i = 0; i < N; i++) {
		for (k = 0; k < N; k++)
		{
			if (k < i && matrix->arrayM[i][k] != 0) {
				kontrolle = false;
				break;
			}
			else {
				continue;
			}
		}
		if (!kontrolle)
			break;
	}
	if (!kontrolle) {
		printf_s("The matrix isn't lower triangular");
	}
	else {
		printf_s("The matrix is lower triangular");
	}
}




