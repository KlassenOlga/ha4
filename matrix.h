#pragma once
#include <stdio.h>
#include<stdbool.h>
#define N 2
void cleanUp();
struct VectorN{
	float arrayV[N];
};
struct MatrixNN{
	float arrayM[N][N];
};
struct VectorN addVectors(struct VectorN* vector1, struct VectorN* vector2);
struct MatrixNN addMatrices(struct MatrixNN* matrix1, struct MatrixNN* matrix2);
struct VectorN scaleVector(struct VectorN* vector, float scalar);
struct MatrixNN scaleMatrix(struct MatrixNN* matrix, float scalar);
struct VectorN multiplyMatrixVector(struct MatrixNN* matrix, struct VectorN* vector);
struct MatrixNN multiplyMatrices(struct MatrixNN* matrix1, struct MatrixNN*matrix2);
struct VectorN inputVector();
struct MatrixNN inputMatrix();
void outputVector(struct VectorN* vector);
void outputMatrix(struct MatrixNN* matrix);
void isDiagonal(struct MatrixNN*matrix);
void isIdentity(struct MatrixNN* matrix);
void isUpperTriangular(struct MatrixNN* matrix);
void isLowerTriangular(struct MatrixNN* matrix);